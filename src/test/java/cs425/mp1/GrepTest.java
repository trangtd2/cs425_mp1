package cs425.mp1;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class GrepTest {
    private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final String configFile = "./files/config.txt";
    private static final String testResultPath = "./files/test_result/";

    private String[] readFile(FileReader file) throws IOException {
        BufferedReader reader = new BufferedReader(file);
        String[] lines = new String[11];
        for (int i = 0; i < lines.length; i++) {
            lines[i] = reader.readLine();
        }
        reader.close();
        return lines;
    }

    public int[] getActual(String[] lines) {
        int n = lines.length;

        String totalActualLine = lines[n-1];
        /*
        Get element and compare
        */
        int[] actualResult = new int[n];
        for (int i = 0; i < n-1 ; i++) {
            String[] params = lines[i].split(":");
            int matches = Integer.parseInt(params[1].trim());
            actualResult[i] = matches;
        }
        actualResult[n-1] = Integer.parseInt(totalActualLine.split(":")[1].trim());

        return actualResult;
    }

    public int[] getObserved(ArrayList<ResultInfo> res) {
        int n = res.size();
        ResultInfo observedTotalLine = res.get(res.size() - 1);

        int[] observedResult = new int[n];
        List<ResultInfo> sortedResult = res.subList(0, res.size() - 1);
        Collections.sort(sortedResult);
        for (int i = 0; i < n-1; i++) {
            observedResult[i] = sortedResult.get(i).getMatches();
        }
        observedResult[n-1] = observedTotalLine.getMatches();

        return observedResult;
    }

    @RepeatedTest(10)
    @DisplayName("Test frequent normal case")
    void test_normal_case() {
        try {
            Long start = System.currentTimeMillis();
            ArrayList<ResultInfo> res = ClientHandler.start(configFile, "grep -c \"Jan.[0-9]\\+\"");
            Long end = System.currentTimeMillis();
            logger.info("Time executed: "+ (end - start) + " ms");

            String[] lines = readFile(new FileReader(testResultPath + "normal_case.txt"));
            assertArrayEquals(getActual(lines), getObserved(res));

        } catch (IOException e) {
            logger.info(e.toString());
        }

    }

    @RepeatedTest(10)
    @DisplayName("Test rare query pattern")
    void test_rare_case() {
        try {
            Long start = System.currentTimeMillis();
            ArrayList<ResultInfo> res = ClientHandler.start(configFile, "grep -c 9.123.164");
            Long end = System.currentTimeMillis();
            logger.info("Time executed: "+ (end - start) + " ms");

            String[] lines = readFile(new FileReader(testResultPath + "rare_case.txt"));
            assertArrayEquals(getActual(lines), getObserved(res));
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }

    @RepeatedTest(10)
    @DisplayName("Test pattern occurs in one log")
    void test_pattern_occured_once() {
        try {
            Long start = System.currentTimeMillis();
            ArrayList<ResultInfo> res = ClientHandler.start(configFile, "grep -c 244.142.133.185");
            Long end = System.currentTimeMillis();
            logger.info("Time executed: "+ (end - start) + " ms");

            String[] lines = readFile(new FileReader(testResultPath + "match_one.txt"));
            assertArrayEquals(getActual(lines), getObserved(res));
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }

    @RepeatedTest(10)
    @DisplayName("Test pattern occurs in several logs")
    void test_pattern_occured_several_case() {
        try {
            Long start = System.currentTimeMillis();
            ArrayList<ResultInfo> res = ClientHandler.start(configFile, "grep -c DELETE");
            Long end = System.currentTimeMillis();
            logger.info("Time executed: "+ (end - start) + " ms");

            String[] lines = readFile(new FileReader(testResultPath + "match_several.txt"));
            assertArrayEquals(getActual(lines), getObserved(res));
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }

    @RepeatedTest(10)
    @DisplayName("Test pattern occurs in all logs")
    void test_pattern_occured_all_case() {
        try {
            Long start = System.currentTimeMillis();
            ArrayList<ResultInfo> res = ClientHandler.start(configFile, "grep -c http");
            Long end = System.currentTimeMillis();
            logger.info("Time executed: "+ (end - start) + " ms");

            String[] lines = readFile(new FileReader(testResultPath + "match_all.txt"));
            assertArrayEquals(getActual(lines), getObserved(res));
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }

    @RepeatedTest(10)
    @DisplayName("Test non occurred pattern in any logs")
    void test_none_occured_pattern_case() {
        try {
            Long start = System.currentTimeMillis();
            ArrayList<ResultInfo> res = ClientHandler.start(configFile, "grep -c /faux.*/i");
            Long end = System.currentTimeMillis();
            logger.info("Time executed: "+ (end - start) + " ms");

            String[] lines = readFile(new FileReader(testResultPath + "match_none.txt"));
            assertArrayEquals(getActual(lines), getObserved(res));
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }

}

