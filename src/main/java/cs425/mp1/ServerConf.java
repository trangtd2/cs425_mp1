package cs425.mp1;

/**
 * This class is specified configurations for Server
 */
public class ServerConf {
    private final String host;
    private final int port;
    private final String logFile;


    public ServerConf(String host, int port, String logFile) {
        this.host = host;
        this.port = port;
        this.logFile = logFile;
    }

    public String getHost() {
        return host;
    }

    public String getLogFile() {
        return logFile;
    }

    public int getPort() {
        return port;
    }
}
