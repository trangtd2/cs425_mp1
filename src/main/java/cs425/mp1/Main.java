package cs425.mp1;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class Main {

    public static void main(String[] args) {
        /*
        Server side only need input port number
         */
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Main")
                .defaultHelp(true)
                .description("Run grep server.");
        parser.addArgument("-p","--port").nargs("?")
                .setDefault(5000)
                .help("Port to run server");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        int port = Integer.parseInt(ns.getString("port"));
        Server server = new Server(port);
    }
}
