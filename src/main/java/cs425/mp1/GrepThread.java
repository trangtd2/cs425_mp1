package cs425.mp1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * This class is run by server to create log or running thread command
 */
public class GrepThread extends Thread{
    private final ServerConf server;
    private final String pattern;
    private ResultInfo result = null;

    public GrepThread(ServerConf server, String pattern){
        this.server = server;
        this.pattern = pattern;
    }

    public ResultInfo getResult() {
        return result;
    }

    @Override
    public void run() {
        try {
            String host = server.getHost();
            int port = server.getPort();
            String logFile = server.getLogFile();

            Socket socket = new Socket(host, port);
            socket.setSoTimeout(5000);

            /*
            Creat output stream to send to servers
             */
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(pattern + " " + logFile);
//            System.out.println("Connected to server. Running job...");

            /*
            Get input from servers and display results of query
             */
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            String output = (String) inputStream.readObject();

            String[] filePath = logFile.split("/");
            result = new ResultInfo(host, filePath[filePath.length - 1].trim(), Integer.parseInt(output.trim()));
            result.print();

        } catch(IOException | ClassNotFoundException u) {
            if (u instanceof ClassNotFoundException) {
                System.err.println("Client fails to get output of the query.");
            } else
                System.err.println("Error happens when connecting to server " + server.getHost());
//            u.printStackTrace();
        } catch (Exception u) {
            System.err.println("Client fails to get response from server.");
        }
    }
}
