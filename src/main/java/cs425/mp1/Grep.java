package cs425.mp1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Grep {
    public static List<String> run_cmd(String command) throws IOException {
        String[] cmd = {"/bin/sh", "-c", command};
//        System.out.println(cmd);
        ProcessBuilder prc_builder = new ProcessBuilder(cmd);
        Process process = prc_builder.start();
        BufferedReader input = new BufferedReader
                              (new InputStreamReader(process.getInputStream()));
        String line;
        List<String> res = new ArrayList<>();

        System.out.println("Start reading logs!");

        //Fix this to return count of line matches
        while ((line = input.readLine()) != null) {
            res.add(line);
        }
        input.close();
        return res;
    }
}
