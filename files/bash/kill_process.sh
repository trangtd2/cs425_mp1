#!/bin/bash

upperlim=10

for ((i=1; i<=upperlim; i++)); do
        echo "Killing server running on VM $i"
        if [ "$i" -lt 10 ]; then
          ssh maitp2@fa22-cs425-520$i.cs.illinois.edu 'kill $(lsof -t -i:5000)'
        else
          ssh maitp2@fa22-cs425-52$i.cs.illinois.edu 'kill $(lsof -t -i:5000)'
        fi
done