#!/bin/bash

upperlim=10

for ((i=1; i<=upperlim; i++)); do
        echo "Running Server jars file in VM $i"
        if [ "$i" -lt 10 ]; then
          ssh maitp2@fa22-cs425-520$i.cs.illinois.edu 'java -jar /home/maitp2/artifacts/grep_server_jar/grep.jar -p 5000' > /tmp/test.out &
        else
          ssh maitp2@fa22-cs425-52$i.cs.illinois.edu 'java -jar /home/maitp2/artifacts/grep_server_jar/grep.jar -p 5000'  > /tmp/test.out &
        fi
done